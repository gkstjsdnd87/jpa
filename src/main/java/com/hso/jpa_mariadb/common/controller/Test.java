package com.hso.jpa_mariadb.common.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController // 여기는 컨트롤러라고 알려주는 @RestController 어노테이션 사용
@RequestMapping("/api") // 여기로 들어올 path를 지정할 @RequestMapping 어노테이션 사용 localhost:8080/api
public class Test {

    @RequestMapping(method = RequestMethod.GET, path = "/requestGet")   // localhost:8080/api/getRequest
    public String requestGet(){
        return "GET 통신";
    }


    @RequestMapping(method = RequestMethod.POST, path = "/requestPost")   // localhost:8080/api/getRequest
    public String requestsPost(){
        return "POST 통신";
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "/requestDelete")   // localhost:8080/api/getRequest
    public String requestsDelete(){
        return "Delete 통신";
    }

    @RequestMapping(method = RequestMethod.HEAD, path = "/requestHead")   // localhost:8080/api/getRequest
    public String requestHead(){
        return "Head 통신";
    }

    @RequestMapping(method = RequestMethod.OPTIONS, path = "/requestOptions")   // localhost:8080/api/getRequest
    public String requestOptions(){
        return "Options 통신";
    }

    @RequestMapping(method = RequestMethod.PATCH, path = "/requestPatch")   // localhost:8080/api/getRequest
    public String requestPatch(){
        return "Patch 통신";
    }

    @RequestMapping(method = RequestMethod.PUT, path = "/requestPut")   // localhost:8080/api/getRequest
    public String requestPut(){
        return "Put 통신";
    }

    @RequestMapping(method = RequestMethod.TRACE, path = "/requestTrace")   // localhost:8080/api/getRequest
    public String requestTrace(){
        return "Trace 통신";
    }

}
