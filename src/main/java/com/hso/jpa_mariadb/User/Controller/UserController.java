package com.hso.jpa_mariadb.User.Controller;

import com.hso.jpa_mariadb.User.Entity.User;
import com.hso.jpa_mariadb.User.Service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/user")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/join")
    public Object requestJoin(@RequestBody User user){
        return userService.joinUser(user);
    }
}
