package com.hso.jpa_mariadb.User.Repository;

import com.hso.jpa_mariadb.User.Entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User,Long> {

}
