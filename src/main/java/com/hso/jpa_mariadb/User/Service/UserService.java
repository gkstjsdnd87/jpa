package com.hso.jpa_mariadb.User.Service;

import com.hso.jpa_mariadb.User.Entity.User;
import com.hso.jpa_mariadb.User.Repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    public Object joinUser(User vo){
        vo.setRole(100L);
        return userRepository.save(vo);
    }
}
